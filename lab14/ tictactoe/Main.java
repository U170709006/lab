 package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args)  {
		Scanner reader = new Scanner(System.in);

		Board board = new Board();
		
		System.out.println(board);
		while (!board.isEnded()) {
			
			int player = board.getCurrentPlayer();
			
			int row = 0;
			int col = 0;
			boolean invalidRow = false;
			
			/** Get row number **/
			do {
				
				System.out.print("Player "+ player + " enter row number:");
				
				try {
					
					row = reader.nextInt();
					invalidRow = false;
					
				}catch (InputMismatchException ex) {
					
					System.out.println("Invalid row");
					
					/** it's required for resolve of loop problem */
					reader.nextLine();
					
					invalidRow = true;
					
				}
			}while(invalidRow);
			
			
			boolean invalidColumn = false;
			
			/** Get column number */
			do {
				
				System.out.print("Player "+ player + " enter column number:");
				
				try {
					
					col = reader.nextInt();
					invalidColumn = false;
					
				}catch (InputMismatchException ex) {
					
					System.out.println("Invalid column");
					
					/** it's required for resolve of loop problem */
					reader.nextLine();
					
					invalidColumn = true;
					
				}
			}while(invalidColumn);
			
			
			try {
				board.move(row, col);
			}catch(InvalidMoveException ex) {
				System.out.print(ex.getMessage());
			}
			System.out.println(board);
			
			
		}
		
		
		reader.close();
	}



}
