package shapes2d;



public class Circle {
	
	public int radius;
	
	public Circle(int radius) {
		this.radius = radius;
		
	}
	public double area() {
		
		return Math.PI*radius*radius;
		
	}
	
	public String toString() {
		return "Radius= " + radius;
	}
	

}
