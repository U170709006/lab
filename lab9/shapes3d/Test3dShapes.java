package shapes3d;

public class Test3dShapes {
	
	public static void main(String[] args) {
		Cylinder cyl = new Cylinder(5,10);
		System.out.println(cyl.area());
		System.out.println(cyl);
		
		Cube cube = new Cube(5);
		System.out.println(cube.area());
		
		
	}

}
